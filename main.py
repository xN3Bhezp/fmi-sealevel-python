from fmiopendata.wfs import download_stored_query
import json, requests, datetime


# Retrieve the latest hour of data from a bounding box
end_time = datetime.datetime.utcnow() + datetime.timedelta(hours=24)
start_time = datetime.datetime.utcnow()
# Convert times to properly formatted strings
start_time = start_time.isoformat(timespec="seconds") + "Z"
# -> 2020-07-07T12:00:00Z
end_time = end_time.isoformat(timespec="seconds") + "Z"
# -> 2020-07-07T13:00:00Z

obs = download_stored_query("fmi::forecast::oaas::sealevel::point::multipointcoverage",
                            args=["bbox=18,55,35,75",
                                  "starttime=" + start_time,
                                  "endtime=" + end_time])

warning_level = 20
shutoff_level = 35
url="http://192.168.100.25/relay/0"

# Check power status
power = requests.get(url).json()['ison']


# Get next 24h hourly sea level forecast and store it in a list
a = [({'timestamp': i.strftime('%Y-%m-%d %H:%M:%S'), 
    'level': obs.data[i]["Pietarsaaren Golf"]['Water level']['value']}) 
    for i in obs.data.keys()]

# Find max value and its timestamp
max_level = max([x['level'] for x in a])
timestamp = datetime.datetime.strptime([x['timestamp'] for x in a if x['level'] == max_level][0], "%Y-%m-%d %H:%M:%S")

# Lawn mover robot operating time is 1700 - 1800. Check if value is above trigger level to decide if power should be shut off. 
if power or not power:
    if max_level > shutoff_level:
        print(f"Powering off as water level will rise today above shutoff threshold {shutoff_level} on {timestamp}.")
        json = {'turn': 'off'}
        requests.post(url, params=json)
    elif max_level > warning_level < shutoff_level:
        print(f"Water will be high today, but below max level, will not power off.")
        json = {'turn': 'on'}
        requests.post(url, params=json)
    else:
        print("no panic")
        json = {'turn': 'on'}
        requests.post(url, params=json)


